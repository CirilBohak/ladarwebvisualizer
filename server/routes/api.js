/*
		Ladar Veb Visualizer

		@author: Ciril Bohak
		@institution: University of Ljubljana,
									Faculty of Computer and Information Science,
									Laboratory of Computer Graphics and Multimedia
		@date: July - October 2017
		@location: Daegu, The Republic of Korea
		@licence: BSD

		This is part of the Ladar Web Visualizar applicaiton developed
		for real-time pointcloud visualization in web browser, where
		data can be obtained from Postgre SQL database or from dedicated
		pint sender application.

		This file defines the application URL routing.
*/

const express = require('express');
const router = express.Router();

const API = 'https://jsonplaceholder.typicode.com';
const { Pool } = require('pg')

// DB connection parameters:
var dbconfig = {
	user: "postgres",
	password: "ladar",
	database: "ladardb",
	port: 5432
};

// Empty set for storing DB queries for later reuse
const queries = {};

// Query prototype
const patchQuery = {
	name: 'fetch-patch-nth',
	text: `
				SELECT
					inp2.x as x,
					inp2.y as y,
					inp2.z as z
				FROM
						(SELECT
								row_number() over() as rn,
								inp.x,
								inp.y,
								inp.z
						FROM
								(SELECT
										PC_Get(PC_Explode(pa), 'x') as x, 
										PC_Get(PC_Explode(pa), 'y') as y, 
										PC_Get(PC_Explode(pa), 'z') as z
								FROM __database_name_placeholder__ WHERE id BETWEEN $1 AND ($1 + $2 - 1)
								) as inp
						) as inp2
				WHERE inp2.rn % $3 = $4`,
	values: [0, 1, 1, 0]
};

// Creating pool of DB connections for parrallel querying.
const pool = new Pool(dbconfig)
pool.connect();

// GET api listing.
router.get('/', (req, res) => {
	res.send('api works');
});

// Get a patch or more of them from PostgreSQL DB
router.get('/patches/:dbname/:patchId/:limit/:stride/:strideOffset', (req, res) => {
	let params = {};
	params.dbname = req.params.dbname == "" ? "las_sim_data_2" : req.params.dbname;
	params.patchId = isNaN(parseInt(req.params.patchId)) ? 1 : req.params.patchId;
	params.limit = isNaN(parseInt(req.params.limit)) ? 1 : req.params.limit;
	params.stride = isNaN(parseInt(req.params.stride)) ? 1 : req.params.stride;
	params.strideOffset = isNaN(parseInt(req.params.strideOffset)) ? 1 : req.params.strideOffset;
	params.outputtype = "json";

	respondToRequest(params, res);
});

// Get a patch or more of them from PostgreSQL DB
router.get('/patches/:dbname/:patchId/:limit/:stride/:strideOffset/:outputtype', (req, res) => {
	let params = {};
	params.dbname = req.params.dbname == "" ? "las_sim_data_2" : req.params.dbname;
	params.patchId = isNaN(parseInt(req.params.patchId)) ? 1 : req.params.patchId;
	params.limit = isNaN(parseInt(req.params.limit)) ? 1 : req.params.limit;
	params.stride = isNaN(parseInt(req.params.stride)) ? 1 : req.params.stride;
	params.strideOffset = isNaN(parseInt(req.params.strideOffset)) ? 1 : req.params.strideOffset;
	params.outputtype = req.params.outputtype;
	params.outputtype = params.outputtype == "json" ? params.outputtype : params.outputtype == "bin" ? params.outputtype : "json";

	respondToRequest(params, res);
});

// Query building method with DB request and result packaging.
function respondToRequest(params, res) {
	let pqName = 'fetch-patch-nth-' + params.dbname;
	if (queries[pqName] === undefined) {
		let pq = {};
		pq.values = [];
		pq.values[0] = params.patchId;
		pq.values[1] = params.limit;
		pq.values[2] = params.stride;
		pq.values[3] = params.strideOffset;
		pq.text = patchQuery.text.replace("__database_name_placeholder__", params.dbname);
		pq.name = pqName;
		queries[pqName] = pq;
	} else {
		queries[pqName].values[0] = params.patchId;
		queries[pqName].values[1] = params.limit;
		queries[pqName].values[2] = params.stride;
		queries[pqName].values[3] = params.strideOffset;
	}

	pool.query( queries[pqName] )
		.then( function(out) {
			let patches = [];
			if (out.rows && out.rows.length > 0) {
				out.rows.forEach( row => {
					patches.push( parseFloat(row.x) );
					patches.push( parseFloat(row.y) );
					patches.push( parseFloat(row.z) );
				});
			}
			switch (params.outputtype) {
				case "json":
					res.status(200).json(patches);
					break;
				case "bin":
					let floats = new Float32Array(patches);
					console.log("api len " + floats.length);
					res.status(200).end(new Buffer(floats.buffer));
					break;
			}
		})
		.catch( e => console.error( e.stack ));
}

module.exports = router;
