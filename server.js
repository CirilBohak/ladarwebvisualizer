/*
		Ladar Veb Visualizer

		@author: Ciril Bohak
		@institution: University of Ljubljana,
									Faculty of Computer and Information Science,
									Laboratory of Computer Graphics and Multimedia
		@date: July - October 2017
		@location: Daegu, The Republic of Korea
		@licence: BSD

		This is part of the Ladar Web Visualizar applicaiton developed
		for real-time pointcloud visualization in web browser, where
		data can be obtained from Postgre SQL database or from dedicated
		pint sender application.

		This file defines the application URL routing.
*/

// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');


// Get our API routes
const api = require('./server/routes/api');

const app = express();

// Get port from environment and store in Express.
const port = process.env.PORT || '3000';
app.set('port', port);

// Create HTTP server.
const server = http.createServer(app);

// Support variables
class User {
	constructor(uname = "", ip = "", token = this.createUserToken(), socket = null) {
		this.uname = uname;
		this.ip = ip;
		this.token = token || this.createUserToken();
		this.awaitingConnections = false;
		this.socket = socket;
		this.pointsLoading = {};
		this.connectedUsers = [];
		this.selectedGeometryGroups = [];
	}

	createUserToken() {
		let tokenLength = 64;
		let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$%&_";
		let token = "";
		for (let i = 0; i < tokenLength; i++) token += charset[Math.floor(Math.random() * charset.length)]
		return "token-" + Math.floor(Date.now()) + "-" + token;
	}
}
var users = [];
var userID = 0;

// Listen on provided port, on all network interfaces.
server.listen( port, () => console.log('API running on localhost:' + port) );

var io = require('socket.io').listen(server);

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

io.sockets.on('connection', function(socket){
	var address = socket.handshake.address;
	let user = new User("Ladar-User-" + userID++, address);
	user.socket = socket;
	users.push(user);
	console.log('Host connected (address: ' + address + ', token: ' + user.token + ')');
	socket.emit("user-token", user.token);
	let usrs = users.filter( function(elt) { return elt.awaitingConnections === true });
	let userList = [];
	usrs.forEach( usr => { userList.push({ uname: usr.uname, token: usr.token}) });
	io.emit("user-list", JSON.stringify( userList ));

  socket.on("disconnect", () => {
  	users = users.filter( function(elt) { return elt.token !== user.token } );
  	console.log("Host disconnected (" + user.uname + "@" + address + ")");
  });

  socket.on("log", msg => {
  	console.log("Message from client: " + msg);
  	socket.emit("log", "Hello from server.");
  });

  socket.on("user-set", pars => {
  	let params = JSON.parse(pars);
  	user.uname = params.uname;
	});

	socket.on("user-list", par => {
		let usrs = users.filter( function(elt) { return elt.awaitingConnections === true });
		let userList = [];
		usrs.forEach( usr => { userList.push({ uname: usr.uname, token: usr.token}) });
		socket.emit("user-list", JSON.stringify( userList ));
	});

	socket.on("user-sharing", par => {
		let params = JSON.parse(par);
		user.awaitingConnections = params.awaitingConnections;
		user.pointsLoading = params.pointsLoading;

		let usrs = users.filter( function(elt) { return elt.awaitingConnections === true });
		let userList = [];
		usrs.forEach( usr => { userList.push({ uname: usr.uname, token: usr.token}) });

		io.emit("user-list", JSON.stringify( userList ));
	});

	socket.on("get-user-params", par => {
		let params = JSON.parse(par);
		let user = users.filter( function(elt) { return elt.token == params.token })[0];
		let connectingUser = users.filter( function(elt) { return elt.token == params.selfToken })[0];
		user.connectedUsers.push(connectingUser);
		socket.emit("sharing-params", JSON.stringify(user.pointsLoading));
	});

	socket.on("disconnect-user", par => {
		let params = JSON.parse(par);
		let usrToken = params.userToken;
		let hostToken = params.hostToken;
		let hostUser = users.filter( function(elt) { return elt.token == hostToken })[0];
		hostUser.connectedUsers = hostUser.connectedUsers.filter( function(elt) { return elt.token !== usrToken } );
		socket.emit("disconnect-success", "");
	});

	socket.on("start-shared-loading", par => {
		let params = JSON.parse(par);
		let hostUser = users.filter( function(elt) { return elt.token == params.hostToken })[0];
		hostUser.connectedUsers.forEach( usr => {
			usr.socket.emit("remote-loading-start", "");
		});
	});

	socket.on("host-camera-params", par => {
		let params = JSON.parse(par);
		// console.log(par);
		let hostUser = users.filter( function(elt) { return elt.token == params.hostToken })[0];
		hostUser.connectedUsers.forEach( usr => {
			usr.socket.emit("client-camera-params", JSON.stringify(params));
		});
	});

	socket.on("update-selected-geometry", par => {
		let params = JSON.parse(par);
		let hostUser = users.filter( function(elt) { return elt.token == params.hostToken })[0];

		for (var i = 0; i < params.updateGeometry.length; i++) {
			let eltIndex = hostUser.selectedGeometryGroups.indexOf( params.updateGeometry[i] );
			if ( eltIndex === -1 ) {
				hostUser.selectedGeometryGroups.push( params.updateGeometry[i] );
			} else {
				hostUser.selectedGeometryGroups.splice(eltIndex);
			}
		}

		hostUser.connectedUsers.forEach( usr => {
			usr.socket.emit("update-selected-geometry", JSON.stringify({ updatedGeometry: hostUser.selectedGeometryGroups }) );
		});
		hostUser.socket.emit("update-selected-geometry", JSON.stringify({ updatedGeometry: hostUser.selectedGeometryGroups }) );
	});
});



const dgram = require('dgram');
const pointServer = dgram.createSocket('udp4');

const NUM_POINTS = 200;

var fs = require('fs');
var numMsg = 0;

pointServer.on('error', (err) => {
	console.log(`pointServer error:\n${err.stack}`);
	pointServer.close();
});

pointServer.on('message', (msg, rinfo) => {
	numMsg++;
	if (numMsg % 1 == 0) {
		// console.log("pointServer got points! #" + (numMsg * NUM_POINTS));
		io.emit("live", msg);
	}
});

pointServer.on('data', (msg, rinfo) => {
	console.log(`pointServer got data: ${msg} from ${rinfo.address}:${rinfo.port}`);
});

pointServer.on('listening', () => {
	const address = pointServer.address();
	console.log(`pointServer listening ${address.address}:${address.port}`);
});

pointServer.bind(2368);
