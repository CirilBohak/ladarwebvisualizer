# Ladar Web Visualizer

This project is a prototype of real-time web-based visualizazation application.

## Running the app

Run `npm run build` to build the project and run a local instance. Navigate to `http://http://<your_server_ip>:3000/ladar-vis`