import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LadarVisComponent } from './ladar-vis/ladar-vis.component';

import { PointFromDbService } from './point-from-db.service';

// Define the routes
const ROUTES = [
	{
		path: '',
		redirectTo: 'ladar-vis',
		pathMatch: 'full'
	},
	{
		path: 'ladar-vis',
		component: LadarVisComponent
	}
];

@NgModule({
	declarations: [
		AppComponent,
		LadarVisComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		RouterModule.forRoot(ROUTES) // Add routes to the app
	],
	providers: [PointFromDbService],
	bootstrap: [AppComponent]
})
export class AppModule { }
