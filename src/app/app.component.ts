/*
		Ladar Veb Visualizer

		@author: Ciril Bohak
		@institution: University of Ljubljana,
									Faculty of Computer and Information Science,
									Laboratory of Computer Graphics and Multimedia
		@date: July - October 2017
		@location: Daegu, The Republic of Korea
		@licence: BSD

		This is part of the Ladar Web Visualizar applicaiton developed
		for real-time pointcloud visualization in web browser, where
		data can be obtained from Postgre SQL database or from dedicated
		pint sender application.

		This file defines the application URL routing.
*/

import { Component, ViewChild } from '@angular/core';
import { PointFromDbService } from './point-from-db.service';
import * as io from 'socket.io-client';
declare var THREE:any;
declare var $:any;
declare var toastr:any;
declare const Buffer

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent {
	// Global component parameters
	title = 'LADAR Visualizator';
	databases = [
		{id: 1, name: "las_sim_data_2", title: "Simulation data", offset: { x: 506649.7647312604, y: 4161751.9591337573, z: 48.520127138184414}},
		{id: 2, name: "las_import", title: "Autzen", offset: { x: 637289.657180654, y: 849999.7600283812, z: 1687.3954044789962}},
		{id: 3, name: "las_temple_import", title: "Temple data", offset: { x: 506649.7647312604, y: 4161751.9591337573, z: 48.520127138184414}},
		{id: 4, name: "las_new_data", title: "New data", offset: { x: 509767.84, y: 4155230.86, z: 19.55}}
	];
	buttonLoadingText = { start: "Start Loading", stop: "Stop Loading"}
	buttonLoading = this.buttonLoadingText.start;
	pointsLoading;
	token = "";
	sharing = false;
	host = null;
	connected = false;
	live = false;
	mousePosition = new THREE.Vector2(0, 0);

	private socket: any; // The client instance of socket.io

	constructor(private pointFromDbService: PointFromDbService) {
		var self = window["app"] = this;
		window["scene"] = new THREE.Scene();
		window["renderer"] = new THREE.WebGLRenderer();
		window["animate"] = function() {
			let scene = window["scene"];
			let camera = window["camera"];
			let renderer = window["renderer"];
			requestAnimationFrame( window["animate"] );
			renderer.render(scene, camera);
		};
		this.pointsLoading = window["pointsLoading"] = {
			loading : false,
			database : "las_sim_data_2",
			patchId : 1,
			numPatches : 5,
			patchStride : 1,
			progressiveLoading : false,
			patchStrideOffset : 0,
			pointService : this.pointFromDbService,
			loadPointsFunc : function() {
				this.pointService.getPointsFromPatchId(this.database, this.patchId, this.numPatches, this.patchStride, this.patchStrideOffset).subscribe(pts => {
				// this.pointService.getPointsFromPatchIdBinary(this.database, this.patchId, this.numPatches, this.patchStride, this.patchStrideOffset).subscribe(pts => { // BINARY DATA ... NOT WORKING !!!
					// let buf = new Buffer(pts);
					// let pts2 = new Float32Array(buf);
					let pts2 = pts;
					if (!this.loading) return;
					self.addNewPointsFunc( pts2 );
					this.patchId += this.numPatches;
					if (pts2.length > 0){
						this.loadPointsFunc();
					} else {
						if (this.progressiveLoading) {
							console.log( "Pass #:" + (this.patchStrideOffset + 1));
							this.patchStrideOffset += 1;
							this.patchId = 1;
							this.loadPointsFunc();
							if (this.patchStrideOffset >= this.patchStride) {
								console.log("Done Loading!");
								this.loading = false;
								window["app"].updateLoadingButton();
							}
						} else {
							console.log("Done Loading!");
							this.loading = false;
							window["app"].updateLoadingButton();
						}
					}
				});
			}
		};
		window["terrainParms"] = {
			terrain : new THREE.Object3D(),
			hoverMaterial : new THREE.PointsMaterial( { size: 5, sizeAttenuation: false, color: 0x880088 }),
			classifiedMaterial : new THREE.PointsMaterial( { size: 5, sizeAttenuation: false, color: 0xff00ff }),
			selectedMaterial : new THREE.PointsMaterial( { size: 3, sizeAttenuation: false, color: 0xffffff }),
			material : new THREE.ShaderMaterial({
				uniforms: {
					maxDistance: {value: 150.0},
					minDistance: {value: 10.0}
				},
				vertexShader: document.getElementById( 'vertexShader' ).textContent,
				fragmentShader: document.getElementById( 'fragmentShader' ).textContent
			})
		};

		window["mouseAndKeboardParams"] = {
			mouseX : 0,
			mouseY : 0,
			mouseDeltaX : 0,
			mouseDeltaY : 0,
			mousePressed : false,
			translationSpeed : 0.75,
			rotationSpeed : 0.5,
			mouseRotationSpeed : 0.001,
			shiftPressed : false,
			altPressed : false
		};
		window.addEventListener( 'resize', this.onWindowResize, false );
	}

	// Method is executed on component initialization
	ngOnInit() {
		let scene = window["scene"];
		window["camera"] = new THREE.PerspectiveCamera( 75, $(".potree_container").width()/$(".potree_container").height(), 0.1, 100000000 );
		let camera = window["camera"];
		let renderer = window["renderer"];
		let tp = window["terrainParms"];
		
		renderer.setSize( $(".potree_container").width(), $(".potree_container").height() );
		$(".potree_container").append( renderer.domElement );

		camera.position.x = 0.0; camera.position.y = 0.0; camera.position.z = 0.0;

		camera.rotateX(1.489363582852331); camera.rotateY(-0.816124899069073); camera.rotateZ(-0.09448957038916661);

		scene.add( tp.terrain );

		this.setupMouseAndKeyboard();
		window["animate"]();
		this.setupUI();
		this.setupSocket();
	}

	// Method handling click on "Start Loading" button
	getPointsClicked(event) {
		this.pointsLoading.loading = !this.pointsLoading.loading;
		console.log("Point loading: " + (this.pointsLoading.loading ? "ON" : "OFF") );
		if (this.pointsLoading.loading) {
			$("[name='sharing_switch']").bootstrapSwitch("disabled", true);
			$("[name='live_switch']").bootstrapSwitch("disabled", true);
		} else {
			$("[name='sharing_switch']").bootstrapSwitch("disabled", false);
			$("[name='live_switch']").bootstrapSwitch("disabled", false);
		}
		this.updateLoadingButton();
		this.pointsLoading.loadPointsFunc();

		if (this.sharing) {
			this.socket.emit("start-shared-loading", JSON.stringify({ hostToken: this.token }));
		}
	}

	// Method hadling adding newlly obtained points from the server to the current visualization
	addNewPointsFunc(pointArray) {
		let MAX_POINTS_PER_GEOMETRY = 1000;
		let tp = window["terrainParms"];
		let self = window["app"];
		let db_offsets = self.databases.filter( function(elt) { return elt.name === self.pointsLoading.database})[0].offset;
		if (db_offsets == undefined || db_offsets == null) db_offsets = {x:0.0, y:0.0, z:0.0};

		let geometry;

		if (tp.terrain.children.length > 0) geometry = tp.terrain.children[tp.terrain.children.length - 1].geometry;

		if (geometry !== undefined && ( (geometry.drawCount + pointArray.length / 3) <= MAX_POINTS_PER_GEOMETRY) ) {
			let positions = geometry.attributes.position.array;
			let offset = geometry.drawCount * 3;
			geometry.drawCount += pointArray.length / 3;

			for (let i = 0; i < pointArray.length; i++) {
				if (i % 3 == 0) positions[i] = pointArray[i] - db_offsets.x;
				if (i % 3 == 1) positions[i] = pointArray[i] - db_offsets.y;
				if (i % 3 == 2) positions[i] = pointArray[i] - db_offsets.z;
			}

			geometry.setDrawRange( 0, geometry.drawCount );
			geometry.attributes.position.needsUpdate = true;
			
		} else {
			let numPoints = pointArray.length;
			let pointsOffset = 0;

			while (numPoints > 0) {
				let currentBatchLen = numPoints > MAX_POINTS_PER_GEOMETRY * 3 ? MAX_POINTS_PER_GEOMETRY * 3 : numPoints;
				numPoints -= currentBatchLen;

				geometry = new THREE.BufferGeometry();
				geometry.drawCount = currentBatchLen;
	
				let positions = new Float32Array(currentBatchLen);
	
				for (let i = 0; i < currentBatchLen * 3; i++) {
					if (i % 3 == 0) positions[i] = pointArray[pointsOffset + i] - db_offsets.x;
					if (i % 3 == 1) positions[i] = pointArray[pointsOffset + i] - db_offsets.y;
					if (i % 3 == 2) positions[i] = pointArray[pointsOffset + i] - db_offsets.z;
				}

				geometry.uidHashCode = this.hashCode([positions[0], positions[1], positions[2]]);
	
				geometry.setDrawRange( 0, geometry.drawCount );
	
				geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));
				let pointsObj =  new THREE.Points( geometry, tp.material );
				pointsObj.selected = false;
				tp.terrain.add( pointsObj );

				pointsOffset += currentBatchLen;
			}
		}
	}

	// Updating loading parameters on user change
	updatePointsLoading(component) {
		switch(component) {
			case "num_patches":
				this.pointsLoading.numPatches = parseInt( $("#" + component).val() );
				break;
			case 'patch_stride':
				this.pointsLoading.patchStride = parseInt( $("#" + component).val() );
				break;
			case 'progressive_loading':
				this.pointsLoading.progressiveLoading = $("#" + component).bootstrapSwitch('state');
				break;
		}
	}

	// Updating Loading button text according to loading state
	updateLoadingButton() {
		this.buttonLoading = this.pointsLoading.loading ? this.buttonLoadingText.stop : this.buttonLoadingText.start;
	}

	// Updating selection of database. For each database we currently set initial view here. This could be changed to be set according to points obtained from DB
	updateDatabaseSelection(evt) {
		let tp = window["terrainParms"];
		if (this.pointsLoading.loading) this.getPointsClicked(null);

		this.pointsLoading.database = evt;
		// console.log(this.pointsLoading.database);

		let camera = window["camera"];
		switch (evt) {
			case this.databases[0].name:
				// las_sim_data_2
				camera.position.x = 0.0; camera.position.y = 0.0; camera.position.z = 0.0;
				camera.rotation.set(0,0,0); camera.updateMatrix();
				camera.rotateX(1.489363582852331); camera.rotateY(-0.816124899069073); camera.rotateZ(-0.09448957038916661);
				tp.material.uniforms.minDistance.value = 10;
				tp.material.uniforms.maxDistance.value = 150;
				tp.material.uniforms.minDistance.needsUpdate = true;
				tp.material.uniforms.maxDistance.needsUpdate = true;
				break;

			case this.databases[1].name:
				// las_import
				camera.position.x = 0.0; camera.position.y = 0.0; camera.position.z = 0.0;
				camera.rotation.set(0,0,0); camera.updateMatrix();
				tp.material.uniforms.minDistance.value = 500;
				tp.material.uniforms.maxDistance.value = 5000;
				tp.material.uniforms.minDistance.needsUpdate = true;
				tp.material.uniforms.maxDistance.needsUpdate = true;
				break;

			case this.databases[2].name:
				// las_temple_import
				camera.position.x = 0.0; camera.position.y = 0.0; camera.position.z = 0.0;
				camera.rotation.set(0,0,0); camera.updateMatrix();
				camera.rotateX(1.489363582852331); camera.rotateY(-0.816124899069073); camera.rotateZ(-0.09448957038916661);
				tp.material.uniforms.minDistance.value = 10;
				tp.material.uniforms.maxDistance.value = 150;
				tp.material.uniforms.minDistance.needsUpdate = true;
				tp.material.uniforms.maxDistance.needsUpdate = true;
				break;

			case this.databases[3].name:
				// las_new_data
				camera.position.x = -63.32299112322288; camera.position.y = 117.084074449499; camera.position.z = 47.735744262481994;
				camera.rotation.set(0,0,0); camera.updateMatrix();
				camera.rotateX(-0.5219480593534138); camera.rotateY(-1.416846211773604); camera.rotateZ(-2.073280426809086);
				tp.material.uniforms.minDistance.value = 10;
				tp.material.uniforms.maxDistance.value = 750;
				tp.material.uniforms.minDistance.needsUpdate = true;
				tp.material.uniforms.maxDistance.needsUpdate = true;
				break;
		}

		this.resetPointLoading();
	}

	// reseting the loading of points
	resetPointLoading() {
		let tp = window["terrainParms"];
		let scene = window["scene"];

		this.pointsLoading.loading = false;
		this.pointsLoading.patchId = 1;
		this.pointsLoading.numPatches = 5;
		this.pointsLoading.patchStride = 1;
		this.pointsLoading.progressiveLoading = false;
		this.pointsLoading.patchStrideOffset = 0;

		for (let i = tp.terrain.children.length-1; i >= 0; i--) {
			let pts = tp.terrain.children[i];
			tp.terrain.remove(pts);
			pts.geometry.dispose();
		}
		scene.remove(tp.terrain);
		tp.terrain = new THREE.Object3D();
		scene.add(tp.terrain);
	}

	// Mouse and Keyboard handling
	setupMouseAndKeyboard() {
		let self = this;
		let scene = window["scene"];
		let camera = window["camera"];
		let renderer = window["renderer"];
		let mouseAndKeboardParams = window["mouseAndKeboardParams"];
		let tp = window["terrainParms"];

		$(document).keypress(function(e) {
			var event = e || window.event;
			// console.log(event.which);
			if ( event.which ==  97)	camera.translateX(-10 * mouseAndKeboardParams.translationSpeed);
			if ( event.which ==  100)	camera.translateX(+10 * mouseAndKeboardParams.translationSpeed);
			if ( event.which ==  113)	camera.translateY(+10 * mouseAndKeboardParams.translationSpeed);
			if ( event.which ==  101)	camera.translateY(-10 * mouseAndKeboardParams.translationSpeed);
			if ( event.which ==  119)	camera.translateZ(-10 * mouseAndKeboardParams.translationSpeed);
			if ( event.which ==  115)	camera.translateZ(+10 * mouseAndKeboardParams.translationSpeed);

			if ( event.which ==  87)	camera.rotateX(Math.PI / 16 * mouseAndKeboardParams.rotationSpeed);
			if ( event.which ==  83)	camera.rotateX(-Math.PI / 16 * mouseAndKeboardParams.rotationSpeed);
			if ( event.which ==  65)	camera.rotateY(Math.PI / 16 * mouseAndKeboardParams.rotationSpeed);
			if ( event.which ==  68)	camera.rotateY(-Math.PI / 16 * mouseAndKeboardParams.rotationSpeed);
			if ( event.which ==  81)	camera.rotateZ(Math.PI / 16 * mouseAndKeboardParams.rotationSpeed);
			if ( event.which ==  69)	camera.rotateZ(-Math.PI / 16 * mouseAndKeboardParams.rotationSpeed);

			if ( event.which ==  72)	{
				tp.terrain.children.forEach( elt => {
					elt.selected = false;
				});

				tp.terrain.children.forEach(function(elt) {
					if (!elt.selected) elt.material = tp.material;
				});
			}

			if (self.sharing) self.sendCameraParameters();
		});

		$(document).keydown(function(e) {
			if (e.keyCode == 16) {
				mouseAndKeboardParams.shiftPressed = true;
			} else if (e.keyCode == 18) {
				mouseAndKeboardParams.altPressed = true;
			}
		});

		$(document).keyup(function(e) {
			if (e.keyCode == 16) {
				mouseAndKeboardParams.shiftPressed = false;
			} else if (e.keyCode == 18) {
				mouseAndKeboardParams.altPressed = false;
			}
		});

		$(".potree_container").mousemove(function(e) {
			if (mouseAndKeboardParams.mousePressed) {
				mouseAndKeboardParams.mouseDeltaX = mouseAndKeboardParams.mouseX - e.pageX;
				mouseAndKeboardParams.mouseDeltaY = mouseAndKeboardParams.mouseY - e.pageY;
			} else {
				mouseAndKeboardParams.mouseDeltaX = 0;
				mouseAndKeboardParams.mouseDeltaY = 0;
			}

			camera.rotateX(- mouseAndKeboardParams.mouseDeltaY * mouseAndKeboardParams.mouseRotationSpeed);
			camera.rotateY(- mouseAndKeboardParams.mouseDeltaX  * mouseAndKeboardParams.mouseRotationSpeed);

			mouseAndKeboardParams.mouseX = e.pageX;
			mouseAndKeboardParams.mouseY = e.pageY;

			if (self.sharing) self.sendCameraParameters();


			if (!mouseAndKeboardParams.mousePressed) {
				// Raycast intersection detection
				let view = $(".potree_container");
				let offset = view.offset();
				let relX = e.clientX - offset.left;
				let relY = e.clientY - offset.top;
				self.mousePosition.x = 2.0 * (relX / renderer.domElement.clientWidth) - 1.0;
				self.mousePosition.y = 1.0 - 2.0 * (relY / renderer.domElement.clientHeight);

				let raycaster = new THREE.Raycaster();
				raycaster.setFromCamera( self.mousePosition, camera );
				var intersections = raycaster.intersectObjects(tp.terrain.children);

				tp.terrain.children.forEach(function(elt) {
					if (!elt.selected) elt.material = tp.material;
					if (elt.selected) elt.material = tp.selectedMaterial;
				});
				if (intersections.length > 0) {
					if (!mouseAndKeboardParams.shiftPressed) {
						intersections[0].object.material = tp.hoverMaterial; // only mark first hit
					} else {
						for (var j = 0; j < intersections.length;j++ ) {
							intersections[j].object.material = tp.hoverMaterial;
						}
					}
				}
			}
		});

		$(".potree_container").mousedown(function(e) {
				$(this).data("initcoords", { x: e.clientX, y: e.clientY });
		});

		$(".potree_container").click(function(e) {
			var initCoords = $(this).data("initcoords") || { x: 0, y: 0 };

			if (e.clientX === initCoords.x && e.clientY === initCoords.y) {
				// Raycast intersection detection
				let view = $(".potree_container");
				let offset = view.offset();
				let relX = e.clientX - offset.left;
				let relY = e.clientY - offset.top;
				self.mousePosition.x = 2.0 * (relX / renderer.domElement.clientWidth) - 1.0;
				self.mousePosition.y = 1.0 - 2.0 * (relY / renderer.domElement.clientHeight);

				let raycaster = new THREE.Raycaster();
				raycaster.setFromCamera( self.mousePosition, camera );
				var intersections = raycaster.intersectObjects(tp.terrain.children);

				if (intersections.length > 0) {
					if (mouseAndKeboardParams.shiftPressed) {
						for (var j = 0; j < intersections.length;j++ ) {
							intersections[j].object.selected = true;
						}
					} else if(mouseAndKeboardParams.altPressed) {
						for (var j = 0; j < intersections.length;j++ ) {
							intersections[j].object.selected = false;
						}
					} else {
						intersections[0].object.selected = !intersections[0].object.selected;
					}

					let updateGeometry = [];
					tp.terrain.children.forEach( elt => {
						if (elt.selected) updateGeometry.push(elt.geometry.uidHashCode);
					});
					if (self.host !== null) {
						self.socket.emit("update-selected-geometry", JSON.stringify({ hostToken: self.host.token, updateGeometry: updateGeometry }) );
					} else {
						self.socket.emit("update-selected-geometry", JSON.stringify({ hostToken: self.token, updateGeometry: updateGeometry }) );
					}
				}
			}
		});

		$('.potree_container').bind('mousewheel', function(e) {
			if (mouseAndKeboardParams.shiftPressed) {
				if (e.originalEvent.wheelDelta / 120 > 0) {
					mouseAndKeboardParams.translationSpeed *= 1.1;
					window["app"].updateToastrMessage("translationSpeed", mouseAndKeboardParams.translationSpeed.toFixed(2));
				} else {
					mouseAndKeboardParams.translationSpeed /= 1.1;
					window["app"].updateToastrMessage("translationSpeed", mouseAndKeboardParams.translationSpeed.toFixed(2));
				}
			} else if (mouseAndKeboardParams.altPressed) {
				if (e.originalEvent.wheelDelta / 120 > 0) {
					mouseAndKeboardParams.rotationSpeed *= 1.1;
					window["app"].updateToastrMessage("rotationSpeed", mouseAndKeboardParams.rotationSpeed.toFixed(2));
				} else {
					mouseAndKeboardParams.rotationSpeed /= 1.1;
					window["app"].updateToastrMessage("rotationSpeed", mouseAndKeboardParams.rotationSpeed.toFixed(2));
				}
			} else {
				if (e.originalEvent.wheelDelta / 120 > 0) {
					camera.translateZ(-10 * mouseAndKeboardParams.translationSpeed / 10.0);
				} else {
					camera.translateZ(+10 * mouseAndKeboardParams.translationSpeed / 10.0);
				}
			}
		});

		$(".potree_container").mousedown(function(e) {
			mouseAndKeboardParams.mousePressed = true;
		});

		$(".potree_container").mouseup(function(e) {
			mouseAndKeboardParams.mousePressed = false;
		});
	}

	// Setting up UI elements that have to be set from code
	setupUI() {
		var self = this;
		let camera = window["camera"];
		let tp = window["terrainParms"];
		// Switch setup
		$.fn.bootstrapSwitch.defaults.size = 'mini';
		$("[name='progressive_loading']").bootstrapSwitch();
		$("[name='progressive_loading']").bootstrapSwitch('state', this.pointsLoading.progressiveLoading);
		$("[name='progressive_loading']").on('switchChange.bootstrapSwitch', function(event, state) {
			window["app"].updatePointsLoading("progressive_loading");
		});

		// Sharing switch setup
		$("[name='sharing_switch']").bootstrapSwitch();
		$("[name='sharing_switch']").on('switchChange.bootstrapSwitch', function(event, state) {
			let sharing = $("[name='sharing_switch']").bootstrapSwitch('state');
			self.resetPointLoading();
			let pl = {
				database : self.pointsLoading.database,
				patchId : self.pointsLoading.patchId,
				numPatches : self.pointsLoading.numPatches,
				patchStride : self.pointsLoading.patchStride,
				progressiveLoading : self.pointsLoading.progressiveLoading,
				patchStrideOffset : self.pointsLoading.patchStrideOffset
			}
			self.socket.emit("user-sharing", JSON.stringify({awaitingConnections: sharing, pointsLoading: pl }) );
			self.sharing = sharing;
			self.toggleFormSharingIneraction();
		});

		// Live switch setup
		$("[name='live_switch']").bootstrapSwitch();
		$("[name='live_switch']").on('switchChange.bootstrapSwitch', function(event, state) {
			let live = $("[name='live_switch']").bootstrapSwitch('state');

			if (self.pointsLoading.loading) self.getPointsClicked(null);
			if (self.pointsLoading.sharing) {
				self.sharing = false;
				self.toggleFormSharingIneraction();
			}

			self.resetPointLoading();
			if (live) {
				// Mean x: 218.41862401466935
				// Mean y: -43.278987868957394
				// Mean z: 829.8947771316733
				camera.position.x = -123.52301446579305;
				camera.position.y = 443.70337029903965;
				camera.position.z = 140.55626752275762;
				tp.material.uniforms.minDistance.value = 50;
				tp.material.uniforms.maxDistance.value = 1000;
				tp.material.uniforms.minDistance.needsUpdate = true;
				tp.material.uniforms.maxDistance.needsUpdate = true;
			} else {
				self.updateDatabaseSelection(self.pointsLoading.database);
			}
			// TODO: send only to subscribed users (client + server)
			self.live = live;
			self.toggleFormLiveIneraction();
		});

		// Help setup
		$("#help").hide();
		$("#help_title").mouseenter(function() {
			$("#help").show("slow");
		});
		$("#help").mouseleave(function() {
			$("#help").hide("slow");
		});
	}

	// Setup the socket connection to server for obtaining the points and for sharing purposes
	setupSocket() {
		var self = this;
		let tp = window["terrainParms"];

		this.socket = io();

		this.socket.on("log", msg => {
			console.log("Message from server: " + msg);
		});

		this.socket.on("user-token", token => {
			this.token = token;
			console.log("Handshake successfull!");
		});

		this.socket.on("user-list", usersJSON => {
			let users = JSON.parse(usersJSON);
			// users.forEach( elt => console.log( elt ));
			$("#user-list").html("").click()
			if (!self.sharing) {
				users.forEach(elt => {
					if (elt.token !== self.token) {
						$("#user-list").append(`
							<button id="connect_to_${elt.uname}" type="button" class="btn btn-block btn-primary btn-sm">
								${elt.uname}
							</button>`
						);
						$("#connect_to_" + elt.uname).click({uname: elt.uname, token: elt.token}, self.connectToHost);
					}
				});
			}
		});

		this.socket.on("sharing-params", par => {
			toastr.info("Connected to host.");
			self.connected = true;
			let pl = JSON.parse(par);
			this.pointsLoading.database = pl.database;
			this.pointsLoading.patchId = pl.patchId;
			this.pointsLoading.numPatches = pl.numPatches;
			this.pointsLoading.patchStride = pl.patchStride;
			this.pointsLoading.progressiveLoading = pl.progressiveLoading;
			this.pointsLoading.patchStrideOffset = pl.patchStrideOffset;
			self.toggleFormEnabled();
		});

		this.socket.on("remote-loading-start", par => {
			self.getPointsClicked(null);
		});

		this.socket.on("disconnect-success", par => {
			toastr.info("Disconnected from the host.");
			self.connected = false;
			self.toggleFormEnabled();
			self.host = null;
		});

		this.socket.on("client-camera-params", par => {
			let params = JSON.parse(par);
			// console.log(params);
			self.setCameraParameters(params);
		});

		this.socket.on("live", par => {
			if (self.live) {
					let NUM_POINTS = 200;
					let buf = new Buffer(par);
					// console.log(buf.length);
		
					let offset = 0;
					let points = [];
					for (let i = 0; i < NUM_POINTS; i++) {
						points.push( buf.readFloatBE(offset).toFixed(8) );
						points.push( buf.readFloatBE(offset+8).toFixed(8) ); // y and z are switched in sender app
						points.push( buf.readFloatBE(offset+4).toFixed(8) );
		
						// Intensity is 0 on sender app
						// point[3] = buf.readInt16BE(offset+12);
						offset += 14;
					}
					self.addNewPointsFunc(points);
				}
		});

		this.socket.on("update-selected-geometry", par => {
			let params = JSON.parse(par);
			// console.log(params);

			tp.terrain.children.forEach( elt => {
				elt.selected = false;
				if (params.updatedGeometry.indexOf(elt.geometry.uidHashCode) !== -1) {
					elt.selected = true;
				} else {
					elt.selected = false;
				}
			});
		});
	}

	// Connect to server
	connectToHost(event) {
		let self = window["app"];
		let socket = window["app"].socket;
		// toastr.info("You got me! " + event.data.uname + " " + event.data.token);
		if (!self.connected) {
			self.host = {uname: event.data.uname, token: event.data.token};
			socket.emit("get-user-params", JSON.stringify({token: event.data.token, uname: event.data.uname, selfToken: self.token}));
		} else {
			socket.emit("disconnect-user", JSON.stringify( { userToken: self.token, hostToken: self.host.token } ));
		}
	}

	// Sending camera parameters to server
	sendCameraParameters() {
		let self = window["app"];
		let camera = window["camera"];

		let cameraParameters = {
			hostToken: self.token,
			position: {
				x: camera.position.x,
				y: camera.position.y,
				z: camera.position.z
			},
			rotation: {
				x: camera.rotation.x,
				y: camera.rotation.y,
				z: camera.rotation.z
			}
		};
		self.socket.emit("host-camera-params", JSON.stringify(cameraParameters));
	}

	// set camera parameters to the obtained parameters
	setCameraParameters(camParams) {
		let camera = window["camera"];

		camera.position.x = camParams.position.x;
		camera.position.y = camParams.position.y;
		camera.position.z = camParams.position.z;
		
		camera.rotation.set(0,0,0); camera.updateMatrix();
		camera.rotateX(camParams.rotation.x);
		camera.rotateY(camParams.rotation.y);
		camera.rotateZ(camParams.rotation.z);
		
	}

	// Updating UI for Live mode
	toggleFormLiveIneraction() {
		if (this.live) {
			toastr.info("Live enabled!");
			$("#selected_database").prop("disabled", true);
			$("#num_patches").prop("disabled", true);
			$("#patch_stride").prop("disabled", true);
			$("[name='sharing_switch']").bootstrapSwitch("disabled", true);
			$("[name='progressive_loading']").bootstrapSwitch("disabled", true);
			$("#loading_button").prop("disabled", true);
		} else {
			toastr.info("Live disabled!");
			$("#selected_database").prop("disabled", false);
			$("#num_patches").prop("disabled", false);
			$("#patch_stride").prop("disabled", false);
			$("[name='sharing_switch']").bootstrapSwitch("disabled", false);
			$("[name='progressive_loading']").bootstrapSwitch("disabled", false);
			$("#loading_button").prop("disabled", false);
		}
	}

	// Updating UI for sharing mode
	toggleFormSharingIneraction() {
		if (this.sharing) {
			toastr.info("Sharing enabled!");
			$("#selected_database").prop("disabled", true);
			$("#num_patches").prop("disabled", true);
			$("#patch_stride").prop("disabled", true);
			$("[name='live_switch']").bootstrapSwitch("disabled", true);
			$("[name='progressive_loading']").bootstrapSwitch("disabled", true);
		} else {
			toastr.info("Sharing disabled!");
			$("#selected_database").prop("disabled", false);
			$("#num_patches").prop("disabled", false);
			$("#patch_stride").prop("disabled", false);
			$("[name='live_switch']").bootstrapSwitch("disabled", false);
			$("[name='progressive_loading']").bootstrapSwitch("disabled", false);
		}
	}

	// Enable/Disable loading controls (when connected to other user)
	toggleFormEnabled() {
		let self = window["app"];
		if (this.connected) {
			// toastr.info("Sharing enabled!");
			$("#connect_to_" + self.host.uname).removeClass("btn-primary").addClass("btn-success");
			$("#selected_database").prop("disabled", true);
			$("#num_patches").prop("disabled", true);
			$("#patch_stride").prop("disabled", true);
			$("[name='progressive_loading']").bootstrapSwitch("disabled", true);
			$("[name='sharing_switch']").bootstrapSwitch("disabled", true);
			$("[name='live_switch']").bootstrapSwitch("disabled", true);
			$("#loading_button").prop("disabled", true);
		} else {
			// toastr.info("Sharing disabled!");
			$("#connect_to_" + self.host.uname).removeClass("btn-success").addClass("btn-primary");
			$("#selected_database").prop("disabled", false);
			$("#num_patches").prop("disabled", false);
			$("#patch_stride").prop("disabled", false);
			$("[name='progressive_loading']").bootstrapSwitch("disabled", false);
			$("[name='sharing_switch']").bootstrapSwitch("disabled", false);
			$("[name='live_switch']").bootstrapSwitch("disabled", false);
			$("#loading_button").prop("disabled", false);
		}
	}

	// Display toast message
	updateToastrMessage(parameter, value) {
		if ($(".toast-message").html() == undefined) {
			toastr.info("Parameter " + parameter + ": " + value);
		} else {
			$(".toast-message").html("Parameter " + parameter + ": " + value);
		}
	}

	// halper method for generating hash of float array
	hashCode(arr) {
		if (arr == null) return 0; 
		var hash = 1;
		for (var i = 0; i < arr.length; i++) {
				hash = hash * 31 + arr[i]
				hash = hash % 0x80000000 // taking only the rightmost 31 bits in integer representation
		}
		return hash;
	}

	// Update thing when window is resized
	onWindowResize(){
		let camera = window["camera"];
		let renderer = window["renderer"];

		camera.aspect = $(".potree_container").width()/$(".potree_container").height();
		camera.updateProjectionMatrix();

		renderer.setSize( $(".potree_container").width(), $(".potree_container").height() );
	}
}
