import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-ladar-vis',
  templateUrl: './ladar-vis.component.html',
  styleUrls: ['./ladar-vis.component.css']
})
export class LadarVisComponent implements OnInit {
  
  constructor(private titleService: Title) { }

  ngOnInit() {
  	this.setTitle("Ladar Visualization");
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

}
