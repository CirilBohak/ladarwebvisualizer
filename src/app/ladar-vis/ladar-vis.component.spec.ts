import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LadarVisComponent } from './ladar-vis.component';

describe('LadarVisComponent', () => {
  let component: LadarVisComponent;
  let fixture: ComponentFixture<LadarVisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LadarVisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LadarVisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
