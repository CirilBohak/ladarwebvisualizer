import { TestBed, inject } from '@angular/core/testing';

import { PointFromDbService } from './point-from-db.service';

describe('PointFromDbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PointFromDbService]
    });
  });

  it('should be created', inject([PointFromDbService], (service: PointFromDbService) => {
    expect(service).toBeTruthy();
  }));
});
