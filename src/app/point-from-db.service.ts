/*
		Ladar Veb Visualizer

		@author: Ciril Bohak
		@institution: University of Ljubljana,
									Faculty of Computer and Information Science,
									Laboratory of Computer Graphics and Multimedia
		@date: July - October 2017
		@location: Daegu, The Republic of Korea
		@licence: BSD

		This is part of the Ladar Web Visualizar applicaiton developed
		for real-time pointcloud visualization in web browser, where
		data can be obtained from Postgre SQL database or from dedicated
		pint sender application.

		This file defines the application URL routing.
*/

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PointFromDbService {

	constructor(private http: Http) { }

	// Get points from patch with certain ID
	getPointsFromPatchId(database, patchId, limit, stride, strideOffset) {
		// console.log("Service #1 loading patchid: " + patchId + " limit: " + limit);
		database = database == "" ? "las_sim_data_2" : database;
		patchId = isNaN(parseInt(patchId)) ? 1 : patchId;
		limit = isNaN(parseInt(limit)) ? 1 : limit;
		stride = isNaN(parseInt(stride)) ? 1 : stride;
		strideOffset = isNaN(parseInt(strideOffset)) ? 1 : strideOffset;
		// console.log("Service #2 loading patchid: " + patchId + " limit: " + limit);

		return this.http.get("/api/patches/"+database+"/"+patchId+"/"+limit+"/"+stride+"/"+strideOffset)
			.map(res => res.json());
	}


	// Get points from patch with certain ID in binary form
	getPointsFromPatchIdBinary(database, patchId, limit, stride, strideOffset) {
		// console.log("Service #1 loading patchid: " + patchId + " limit: " + limit);
		database = database == "" ? "las_sim_data_2" : database;
		patchId = isNaN(parseInt(patchId)) ? 1 : patchId;
		limit = isNaN(parseInt(limit)) ? 1 : limit;
		stride = isNaN(parseInt(stride)) ? 1 : stride;
		strideOffset = isNaN(parseInt(strideOffset)) ? 1 : strideOffset;
		// console.log("Service #2 loading patchid: " + patchId + " limit: " + limit);

    return this.http.get("/api/patches/"+database+"/"+patchId+"/"+limit+"/"+stride+"/"+strideOffset+"/bin")
    	.map(res => res.arrayBuffer());

		// return this.http.get("/api/patches/"+database+"/"+patchId+"/"+limit+"/"+stride+"/"+strideOffset+"/bin");
	}
}
